/* -----------------------------------------
  Vanilla JS Selector
 -----------------------------------------*/
 function S(querySelector) {
  return document.querySelector(querySelector);
}



/*--------------------------------------------------
	Video
--------------------------------------------------*/
Vue.component('lydos-video', {
  template: '\
		<div class="lydos-video" @click="play()">\
      <div class="button-wrapper">\
			  <button class="button button-play" :class="{hide: isPlaying}">play video</button>\
      </div>\
			<video class="aside-video" :poster="poster" preload="none"> \
				<source :src="mp4" type="video/mp4"> \
				<source :src="ogg" type="video/ogg"> \
				<source :src="webm" type="video/webm"> \
				Your browser does not support the video tag. \
			</video> \
		</div> \
	',
  props: ['mp4', 'ogg', 'webm', 'poster'],
  data: function () {
    return {
      isPlaying: false
    };
  },
  computed: {
    video: function () {
      return this.$el.querySelector('video');
    }
  },
  methods: {
    play: function () {
			var vm = this; 
      this.video.play();
      this.video.controls = true;
			this.isPlaying = true;
			
			/*
			// PER TEST CARICAMENTO VIDEO
			this.video.addEventListener('play', function () {
				try {
					$('#log')
						.append(vm.video.buffered.length + '<br>')
						.append(vm.video.buffered.start(0) + '<br>')
						.append(vm.video.buffered.end(0) + '<br>');

					//console.log(vm.video.buffered.length);
					// console.log(vm.video.buffered.start(0));
					// console.log(vm.video.buffered.end(0));
				} catch (err) {
					console.log('Error logging');
				}
			});
			*/

			
    }
  },
  mounted: function () {
    var vm = this;
    vm.video.onended = function () {
      vm.isPlaying = false;
      vm.video.controls = false;
      vm.video.load();
    };
  }

});


/* -----------------------------------------
  Video con trasparenza (non usati)
 -----------------------------------------*/
Vue.component('alpha-video', {
  template: '\
    <div class="alpha-video" :id="\'canvas_output_\' + id">  \
      <video :id="\'video_\' + id" style="display:none" loop autoplay preload="none"> \
        <source :src="src" type="video/mp4;" /> \
      </video> \
      <canvas :width="width" :height="height*2" :id="\'buffer_\' + id" style="display: none"></canvas> \
      <canvas :width="width" :height="height" :id="\'output_\' + id"></canvas> \
    </div>',
  props: [
    'src', 'width', 'height'
  ],
  computed: {
    id: function () {
      return this._uid;
    }
  },
  mounted: function () {

    var vm = this;
    var outputCanvas = document.getElementById('output_' + vm.id),
        output = outputCanvas.getContext('2d'),
        bufferCanvas = document.getElementById('buffer_' + vm.id),
        buffer = bufferCanvas.getContext('2d'),
        video = document.getElementById('video_' + vm.id),
        width = outputCanvas.width,
        height = outputCanvas.height,
        interval
    ;


    function processFrame() {
      buffer.drawImage(video, 0, 0);

      // this can be done without alphaData, except in Firefox which doesn't like it when image is bigger than the canvas
      var image = buffer.getImageData(0, 0, width, height),
      imageData = image.data,
      alphaData = buffer.getImageData(0, height, width, height).data;

      for (var i = 3, len = imageData.length; i < len; i = i + 4) {
        imageData[i] = alphaData[i - 1];
      }

      output.putImageData(image, 0, 0, 0, 0, width, height);
    }


    video.addEventListener('play', function () {
      clearInterval(interval);
      interval = setInterval(processFrame, 25);
    }, false);

    video.addEventListener('ended', function () {
      clearInterval(interval);
      video.stop();
    }, false)

    // Firefox doesn't support looping video, so we emulate it this way
    //video.addEventListener('ended', function () {
    //  video.play();
    //}, false);

    video.play();

  }
});

var overview = new Vue({
  el: '.overview'
});






/* -----------------------------------------
  Discover
 -----------------------------------------*/
new Vue({
  el: '.discover',
  data: {
    currentSlide: 1,
    totalSlides: ''
  },
  computed: {
    // currentSlide
  },
  methods: {
    goPrev: function () {
      $('.discover .slider').slick('slickPrev');
    },
    goNext: function () {
      $('.discover .slider').slick('slickNext');
    },
    zoom: function () {
      $.fancybox.open($('.discover .slider a'))
    }
  },
  mounted: function () {
    var vm = this;

    $('.discover .slider')
			.on('init', function (event, slick) {
			  vm.totalSlides = slick.slideCount;
			})
			.slick({
			  arrows: false,
			  infinite: false
			})
			.on('afterChange', function (event, slick) {
			  vm.currentSlide = slick.currentSlide + 1;
			})
    ;
  }
});








/*--------------------------------------------------
Scroll to hash
--------------------------------------------------*/
function scrollToHash() {
  $('.scroll-to-hash').click(function (ev) {
    ev.preventDefault();
    var self = this;
    var target = document.querySelector(self.href.match(/#[^?]+/ig));
    $('html, body').animate({ scrollTop: target.offsetTop - 50 }, 1200);
  });
}




/* -----------------------------------------
  Calcolatore
-----------------------------------------*/
var calculator = new Vue({
  el: '.calculator',
  data: {
    search: 'replacement',
    inputReplacement: ['current model', 'current capacity', 'Age of water heater', 'number of people'],
    inputNew: ['capacity of your electric water heater', 'number of people'],
    showResult: false,
    slideSpeed: 400,
    options: {
      model: ['m1', 'm2', 'm3'],
      people: ['1-2', '3', '4', '5'],
      age: ['1-2yrs', '3-6yrs', '>6'],
      capacity: ['80 or <80', '100', '80,<80,100']
    }
  },
  methods: {
    staggerElems: function (elem) {
      var items = $(elem + ' .calculator__col');
      TweenMax.staggerFromTo(items, 1, { opacity: 0 }, { opacity: 1 }, 0.1);
    },
    showSection: function (query) {
      var vm = this;
      this.search = query;
      if (this.showResult === false) {
        $('.calculator__replacement, .calculator__new')
          .stop()
          .slideToggle()
        ;
      } else {
        $('.calculator__result').stop().slideUp(vm.slideSpeed, function () {
          $('.calculator__' + query).slideDown();
          vm.showResult = false;
        });
      }
    },
    showResults: function () {
      var vm = this;
      $('.calculator__replacement, .calculator__new').stop().slideUp(vm.slideSpeed);
      $('.calculator__result').stop().slideDown(vm.slideSpeed, function () {
        vm.showResult = true;
      });

    }
  },
  mounted: function () {
    console.log('ok');
  }
});



/* -----------------------------------------
  BG 
 -----------------------------------------*/
function seaBG() {

  function percent(n, percent) { return (n / 100) * percent; }

  // Posiziona le maschere in base all'offset dell'elemento di riferimento
  function setPositions() {

    // 1. Maschera inferiore primo video
    $('.sea-bg__divider-1')[0].style.top = $('.sea-bg__splash').height() - 160 + 'px';

    // 2. Secondo video
    $('.sea-bg__50')[0].style.top = (function () {
      var height = $('.sea-bg__50').outerHeight();
      return $('.everyday__video-placeholder').offset().top - percent(height, 60) + 'px';
    }());

    // 3. Maschera superiore secondo video
    $('.sea-bg__divider-2')[0].style.top = $('.sea-bg__50').offset().top - 160 + 'px';

    // 4. Immagine dopo secondo video
    $('.sea-bg__intelligence')[0].style.top = ($('.sea-bg__50').offset().top + $('.sea-bg__50').outerHeight()) - 110 + 'px';

    // 5. Immagine fondale marino
    $('.sea-bg__sand')[0].style.top = $('.overview .comfort').offset().top - 800 + 'px';



  }

  var throttled = _.throttle(setPositions, 1500);

  $(window)
		.on('resize', throttled)
		.on('load', function () {
		  setTimeout(setPositions, 100);
		})
  ;
}



function fxSetup() {

  // Cover opacity 0 per evitare che si veda troppo presto
  $('.overview .cover')[0].style.opacity = 0;

  // WOW 

  $('.title-xl, .aside-img, .lydos-video, .overview .service').addClass('wow fadeIn');
  $('.cover__boiler').addClass('wow fadeInDown').attr('data-wow-delay', '1s')
  $('.cover .text-content').attr('data-wow-delay', '.5s');
  $('.overview .text-content')
		.addClass('wow fadeInRightSmall')
		.attr('data-wow-delay', '.4s')
  ;
  // $('.everyday header .title').attr('data-wow-delay', '3s');
  $('.everyday__boiler').addClass('wow fadeInLeft').attr('data-wow-delay', '.4s');


  // Cover visibile al load
  $(window).on('load', function () {
		  $('.overview .cover')[0].style.opacity = 1;
	});

}


/* -----------------------------------------
  DOC READY  
 -----------------------------------------*/
$(function () {

  scrollToHash();


  if ($(window).width() > 768) {

    fxSetup();
    new WOW().init();
    seaBG();
    $.stellar({
      horizontalScrolling: false
    });
  }

});

