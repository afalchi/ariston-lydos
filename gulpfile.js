'use strict';

var gulp = require('gulp'),
    plumber = require('gulp-plumber'),
    webserver = require('gulp-webserver'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    autoprefixer = require('gulp-autoprefixer'),
    // tfs = require('gulp-tfs'),
		livereload = require('gulp-livereload'),
		wait = require('gulp-wait'),
    imagemin = require('gulp-imagemin')
;


/***********************
*** TFS GET LATEST ***
************************/
gulp.task('getLatest:all', function () {
  return gulp.src([
      './css/*',
      './sass/**/*'
  ])
      .pipe(tfs.get());
});


/*******************
*** TFS CHECKOUT ***
********************/
gulp.task('checkout', function () {
  return gulp.src([
    './css/main.css'
  ])
  .pipe(tfs.checkout());
});



gulp.task('sass', function () {
  // gulp.task('sass', ['checkout'], function () {
	return gulp.src('./css/sass/**/*.scss')
		// .pipe(wait(500))
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer({
      browsers: ['last 2 versions', 'Explorer >= 10', 'Android >= 4.1', 'Safari >= 7', 'iOS >= 7'],
      cascade: false
    }))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./css'))
    .pipe(livereload())
  ;
});


gulp.task('sass:watch', function () {
  livereload.listen();
  gulp.watch('./css/sass/**/*.scss', ['sass']);
});



/* -------------------------------------------------- 
  Webserver
-------------------------------------------------- */
gulp.task('webserver', function () {
  gulp.src('./')
    .pipe(webserver({
      livereload: true,
      directoryListing: true,
      // host: '192.168.0.157',
      // port: 40010,
      open: true
    }));
});


/* -------------------------------------------------- 
  Watch
-------------------------------------------------- */
gulp.task('watch', function () {
  livereload.listen();
  gulp.watch('js/*.js');
});


/* -------------------------------------------------- 
  Compress images
-------------------------------------------------- */
gulp.task('image-min', function () {
  gulp.src('/images/*')
    .pipe(imagemin())
    .pipe(rename(function (path) {
      //path.dirname += "/ciao";
      path.basename += "-goodbye";
      path.extname = ".md"
    }))
    .pipe(gulp.dest('dist/images'))
  ;
});




/* -------------------------------------------------- 
  Default
-------------------------------------------------- */
gulp.task('default', ['webserver', 'sass', 'sass:watch', 'watch']);
